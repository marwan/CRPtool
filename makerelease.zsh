#!/bin/zsh

# Get a list of existing tags (should be at least "-n 2"!)
tags=$(git tag | sort -V| tail -n 2)

previous_tag=v5.27

# Loop through each tag and create a release
for tag in ${=tags}; do
  echo "Creating release for tag: $tag"

  # Check if release already exists
  response=$(curl -s --header "PRIVATE-TOKEN:_tgyrM3Y6MP6Ey1vTBAN" "https://gitlab.pik-potsdam.de/api/v4/projects/1583/releases/$tag")
  if [[ "$response" != "{\"message\":\"404 Not Found\"}" ]]; then
    echo "Release already exists for tag: $tag. Skipping..."
    previous_tag=$tag
    continue
  fi
  
  # Get the commit messages between the previous and current tags
  commit_messages=$(git log --pretty=format:"- %s" $previous_tag..$tag)
  
  # Create the release using GitLab API and use the commit messages as release notes
  # Replace YOUR_ACCESS_TOKEN with your actual GitLab access token
  response=$(curl -s --header "PRIVATE-TOKEN:_tgyrM3Y6MP6Ey1vTBAN" --data "name=$tag&tag_name=$tag&ref=master&&description=$commit_messages" "https://gitlab.pik-potsdam.de/api/v4/projects/1583/releases")
  
  #echo $response
  
  # Set the previous tag for the next iteration
  previous_tag=$tag
done

#curl -s --header "PRIVATE-TOKEN:_tgyrM3Y6MP6Ey1vTBAN" --data "name=v5.27&tag_name=v5.27&description=$commit_messages" "https://gitlab.pik-potsdam.de/api/v4/projects/1583/releases"
