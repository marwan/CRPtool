function y=normalize(x)
%NORMALIZE   Normalizes data series.
%   Y=NORMALIZE(X) normalizes the matrix X to zero-mean and
%   standard deviation one (Y=(X-mean(X))/std(X)).

% Copyright (c) 2008-
% Norbert Marwan, Potsdam Institute for Climate Impact Research, Germany
% https://tocsy.pik-potsdam.de
%
% Copyright (c) 1998-2008
% Norbert Marwan, Potsdam University, Germany
% http://www.agnld.uni-potsdam.de
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or any later version.


narginchk(1,1)
nargoutchk(0,1)
if min(size(x))==1
    y=(x-nanmean(x))/nanstd(x);
else

  for i=1:size(x,2);
    y(:,i)=(x(:,i)-nanmean(x(:,i)))/nanstd(x(:,i));
  end

end
