#!/bin/zsh


# Replace with your desired output file name
OUTPUT_FILE="changelog.md"

echo "# Revision history\n" > $OUTPUT_FILE

# Get all tags in chronological order
tags=($(git tag --sort=-creatordate))

# Iterate over tags
for i in {1..$#tags}
do
    tag=${tags[$i]}
    prev_tag=${tags[$i+1]}

    # Check if there is a previous tag
    if [ -n "$prev_tag" ]; then
    
        # Get the last commit date before the current tag
        commit_date=$(git log --format="%ad" -n 1 --date=short $prev_tag)

        # Get the commit range between tags
        commit_range="$prev_tag..$tag"

        commit_info=$(git log --pretty=format:"- %s" --reverse --date=short $commit_range)
       # Write the tag and commit messages to the output file if there are non-empty messages
        echo "## $tag (Released on: $commit_date)" >> $OUTPUT_FILE
        echo "$commit_info" | grep -v "^\s*- \*\*\* empty log message \*\*\*" >> $OUTPUT_FILE
        echo >> ../$OUTPUT_FILE


        echo >> $OUTPUT_FILE
    fi
done


##################################

accessToken='uLryxusoxFZ_GweFNtwz'


tagsUrl="https://gitlab.pik-potsdam.de/api/v4/projects/1583/repository/tags"
tagJson=$(curl -s "$tagsUrl")

# Remove newlines
tagJson=$(echo "$tagJson" | tr -d '\n' | jq -r '.[]')




commitsUrl="https://gitlab.pik-potsdam.de/api/v4/projects/1583/repository/commits"

# Initialize an empty array to store all commits
allCommits=""

# Retrieve commits page by page until all commits are fetched
page=1
while true; do
  # Fetch the current page of commits
  commitsJson=$(curl -s "$commitsUrl?page=$page")
    
  # Remove newlines from the current page of commits
  commitsJson=$(echo "$commitsJson" | tr -d '\n')

  # Check if the fetched commits are empty (indicating the end of commits)
  if [ "$(echo "$commitsJson" | jq length)" -eq 0 ]; then
    break
  fi
  
  # Append the page commits to the allCommits array
  pageCommits=$(echo "$commitsJson" | jq -r '.[]')
  allCommits="${allCommits}\n{$pageCommits}"
  
  # Increment the page counter for the next iteration
  page=$((page + 1))
done

data=$(echo "$allCommits" | sed 's/}}/}/g; s/{{/{/g')
  
  
data=$(echo "$data" | jq -c '.')


ids=$(echo $data | jq -r '.short_id')

tag=""
message=""
oldmessage=""


for i in ${(f)ids}; do

   tag=$(echo "$tagJson" | jq -r ". | select(.commit.short_id == \"$i\") | .name ")

  newmessage=$(echo "$data" | jq -r ". | select(.short_id == \"$i\") | .message")
  if [[ "$newmessage" != "*** empty log message ***" && "$newmessage" != "$oldmessage" ]]; then
     message="${message}\n${newmessage}"
  fi
  oldmessage=$newmessage
  
  day=$(echo "$data" | jq -r ". | select(.short_id == \"$i\") | .committed_date")
  if [[ -n "$tag" ]]; then
     echo "\n\n $tag"
     echo $day
     echo $message
     message=""
  fi
done



