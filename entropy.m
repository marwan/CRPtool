function e=entropy(x)
%ENTROPY   Entropy of a distribution.
%    E=ENTROPY(X) computes the entropy of the
%    distribution X.
%
%    See also MI.

% Copyright (c) 2008-
% Norbert Marwan, Potsdam Institute for Climate Impact Research, Germany
% https://tocsy.pik-potsdam.de
%
% Copyright (c) 1999-2008
% Norbert Marwan, Potsdam University, Germany
% http://www.agnld.uni-potsdam.de
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or any later version.

narginchk(1,1)
nargoutchk(0,1)

if isempty(x)
    e = 0;
else
    for j=1:size(x,2);
      x(:,j)=x(:,j)./sum(x(:,j));
      x2=x(find(x(:,j)),j);
      e(:,j)=sum(-x2.*log(x2));
    end
end

